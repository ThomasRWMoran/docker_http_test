#!/bin/bash
docker build -t tommoran/httpd:1.0 .
docker run -itd --name tomweb -P tommoran/httpd:1.0
port=$(docker port tomweb | grep '^80' | awk -F: '{print $NF}')
curl localhost:$port | grep Tom
image_error=0
if [ $? != 0 ]; then
    image_error=1
    echo "Curl failed, image will be deleted"
else
    echo "Curl successful, image will be kept"
fi
docker rm -f tomweb

if [ $image_error == 1 ]; then
    docker rmi tommoran/httpd:1.0
fi
