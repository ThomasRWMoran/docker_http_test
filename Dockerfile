FROM httpd
COPY index.html /usr/local/apache2/htdocs/index.html
EXPOSE 80 443
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install php
